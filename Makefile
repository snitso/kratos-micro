.PHONY: init
# init env
init:
	make -C app/demo init

# generate all
# generate api proto
# generate internal conf proto
# generate all proto, wire etc...
.PHONY: all
proto:
	make -C app/demo all

.PHONY: demo
demo:
	cd app/demo && air -c .air.toml
