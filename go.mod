module kratos

go 1.21

toolchain go1.21.4

require (
	github.com/go-kratos/kratos/v2 v2.6.2
	github.com/google/wire v0.5.0
	go.uber.org/automaxprocs v1.5.3
	google.golang.org/genproto v0.0.0-20230803162519-f966b187b2e5
	google.golang.org/grpc v1.57.0
	google.golang.org/protobuf v1.31.0
)

require (
	dario.cat/mergo v1.0.0 // indirect
	github.com/bep/godartsass v1.2.0 // indirect
	github.com/bep/godartsass/v2 v2.0.0 // indirect
	github.com/bep/golibsass v1.1.1 // indirect
	github.com/cli/safeexec v1.0.1 // indirect
	github.com/cosmtrek/air v1.49.0 // indirect
	github.com/creack/pty v1.1.21 // indirect
	github.com/fatih/color v1.16.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/go-kratos/aegis v0.2.0 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-playground/form/v4 v4.2.0 // indirect
	github.com/gohugoio/hugo v0.120.4 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.1.0 // indirect
	github.com/spf13/afero v1.10.0 // indirect
	github.com/tdewolff/parse/v2 v2.7.5 // indirect
	go.opentelemetry.io/otel v1.7.0 // indirect
	go.opentelemetry.io/otel/trace v1.7.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sync v0.4.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
